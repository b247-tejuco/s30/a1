// Part 1
db.fruits.aggregate(
  [
    {
      $match: {onSale: true}
    },
    {
      $count: "fruitsOnSale"
    }
  ]
)

// Part 2

db.fruits.aggregate(
  [
    {
      $match: {stock: {$gte: 20}}
    },
    {
      $count: "enoughStock"
    }
  ]
)

// Part 3

db.fruits.aggregate(
   [
     {
        $match: {onSale: true}
     },
     {
       $group:
         {
           _id: "$supplier_id",
           avg_price: { $avg: "$price" }
         }
     },
     {
      $sort: {total: -1}
     }
   ]
)

// Part 4

db.fruits.aggregate(
   [
       {
        $match: {onSale: true}
       },
       {
       $group:
         {
           _id: "$supplier_id",
           max_price: { $max: "$price" }
         }
       }
   ]
)

//Part 5

db.fruits.aggregate(
   [
       {
        $match: {onSale: true}
       },
       {
        $group:
         {
           _id: "$supplier_id",
           min_price: { $min: "$price" }
         },
       },
       {
        $sort: {total: -1}
       }    
   ]
)